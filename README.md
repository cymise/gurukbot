# Gurukbot - 할 짓 없어서 만든 디스코드 봇.

## 토큰이나 API 키가 저장되는 파일 리스트
    bot_token.txt -> 디스코드 봇 토큰
    airkorea_key.txt -> 공공데이터포털 API 키

## 사용자 커스텀 명령어 저장 파일
    commands_list.txt

## 필요한 모듈들
    discord.py
    requests
    (이하 파이썬 기본 모듈)
    asyncio
    random
    json
    ElementTree
    sys
    plaform

## 왜 이렇게 못 만들었나요?
    제가 파이썬을 잘 못합니다.
    디스코드 봇 만든것도 처음이야.

## 왜 discord.py를 썼나요? 직접 처음부터 짜야죠!
    위 이유랑 같습니다.

## 기타
    python3.6 그리고 3.7에서 작동합니다.
    
## 라이센스
    BSD 3-Clause License
    