# -*_ coding: utf-8 -*-
#구룩이봇 유튜브 확장

import os
import random
import string
import youtube_dl
from datetime import datetime

main_dir = "/srv/dev-disk-by-id-ata-WDC_WD20PURZ-85GU6Y0_WD-WCC4M1KADCTP-part1/gurugiga/ytdl/"
server_url = "https://k-bird.kr/ytdl/"
current_filename = ""

def dlhook(dl):
    global current_filename
    if dl['status'] == 'finished':
        file_tuple = os.path.split(os.path.abspath(dl['filename']))
        current_filename = file_tuple[1]

def search_file(fname):
    global current_filename
    ymd = '{0}-{1}-{2}'.format(datetime.today().year, datetime.today().month, datetime.today().day)
    path = main_dir + '/' + ymd
    file_names = os.listdir(path)
    
    for name in file_names:
        if name.startswith(fname):
            break
    current_filename = name

 
def dload(video_url, opt):
    ymd = '{0}-{1}-{2}'.format(datetime.today().year, datetime.today().month, datetime.today().day)
    randomname = "".join([random.choice(string.ascii_letters) for _ in range(10)])
    dl_dir = main_dir + '/' + ymd + '/' + randomname + '.%(ext)s'
    global current_filename

    ydl_opts_audio_normal = {
    'format': 'best/best',
    'default_search': 'auto',
    'quiet': True,
    'no_warnings': True,
    'noplaylist': True,
    'outtmpl': dl_dir,
    #'progress_hooks': [dlhook],
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',

    }],
    }

    ydl_opts_audio_best = {
    'format': 'bestaudio/best',
    'default_search': 'auto',
    'quiet': True,
    'no_warnings': True,
    'noplaylist': True,
    'outtmpl': dl_dir,
    #'progress_hooks': [dlhook],
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '320',

    }],
    }

    ydl_opts_video_normal = {
    'format': 'best/best',
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'noplaylist': True,
    #'progress_hooks': [dlhook],
    'outtmpl': dl_dir,
    }

    ydl_opts_video_best = {
    'format': 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4',
    'default_search': 'auto',
    'quiet': True,
    'no_warnings': True,
    'noplaylist': True,
    #'progress_hooks': [dlhook],
    'outtmpl': dl_dir,
    }

    if (opt==0):
        with youtube_dl.YoutubeDL(ydl_opts_audio_normal) as ydl:
            ydl.download([video_url])
        
        search_file(randomname)
        return server_url + ymd + '/' + current_filename
    elif (opt==1):
        with youtube_dl.YoutubeDL(ydl_opts_audio_best) as ydl:
            ydl.download([video_url])

        search_file(randomname)
        return server_url + ymd + '/' + current_filename
    elif (opt==2):
        with youtube_dl.YoutubeDL(ydl_opts_video_normal) as ydl:
            ydl.download([video_url])

        search_file(randomname)
        return server_url + ymd + '/' + current_filename
    elif (opt==3):
        with youtube_dl.YoutubeDL(ydl_opts_video_best) as ydl:
            ydl.download([video_url])

        search_file(randomname)
        return server_url + ymd + '/' + current_filename

def search(video_url):
    ydl_opts_search = {
    'format': 'bestaudio',
    'default_search': 'auto',
    'quiet': True,
    'noplaylist': True,
    #'progress_hooks': [dlhook],
    }
    with youtube_dl.YoutubeDL(ydl_opts_search) as ydl:
        try:
            vid_info = ydl.extract_info(video_url, download = False)['entries'][0]
        except:
            vid_info = ydl.extract_info(video_url, download = False)

    vid_url = vid_info['webpage_url']
    vid_title = vid_info['title']

    return [vid_title, vid_url]

def clean():
    cmdline = "rm -r " + main_dir + "*"
    os.system(cmdline)

