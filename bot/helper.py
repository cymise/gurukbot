# -*_ coding: utf-8 -*-
#구룩이봇 도움말 표시 모듈(helper)
import ReadEngine

help_string = ""
cmd_list = {}
adm_string = ""

def load():
    global cmd_list
    global help_string
    global adm_string
    help_string = ReadEngine.Read_To_String("./db/help.txt")
    cmd_list = ReadEngine.Read_To_Diction("./db/help_cmd.txt")
    adm_string = ReadEngine.Read_To_String("./db/help_adm.txt")

def help_str():
    global help_string
    return help_string

def cmd_help(cmd):
    global cmd_list
    
    if (cmd in cmd_list):
        return cmd_list[cmd]
    else:
        return "-1"

def adm_help(cmd):
    global adm_string
    return adm_string

def cmd_help_write(cmd, string):
    global cmd_list
    ReadEngine.Add_To_Text("./db/help_cmd.txt", cmd, string)
    load()
    return 0

def cmd_help_delete(cmd):
    global cmd_list
    if (cmd in cmd_list):
        cmd_list.pop(cmd)
        ReadEngine.Write_Diction_To_Text("./db/help_cmd.txt", cmd_list)
        load()
        return 0
    else:
        return -1


load()