# -*_ coding: utf-8 -*-

from googletrans import Translator

translator = Translator()

def translate(string: str, destination: str):
    result = translator.translate(string, dest = destination)
    return result.text