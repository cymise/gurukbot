# -*_ coding: utf-8 -*-
#(ReadEngine) Test

import ReadEngine

imdict = {"AAA":"a\na\na", "BBB":"bb", "CCC":"ccc"}
whyimdict = {}

ReadEngine.Add_To_Text("./test.txt", "HEllo!", "Hi!!!!")
ReadEngine.Add_To_Text("./test.txt", "Bye", "Bi!!!!")
ReadEngine.Add_To_Text("./test.txt", "LOL", "LOL!!!!")

ReadEngine.Write_Diction_To_Text("./dicttest.txt", imdict)
whyimdict = ReadEngine.Read_To_Diction("./dicttest.txt")
print(whyimdict)