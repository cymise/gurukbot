import random
import string
from datetime import datetime

filedir = "./urls/"
html_body = "<html><meta http-equiv=\"refresh\" content=\"0; url={0}\"></meta></html>"

def make_short(url: str):
    random.seed(datetime.now())
    key_length = random.randint(2, 4)
    key = "".join([random.choice(string.ascii_lowercase) for _ in range(key_length)])

    if not(url.startswith("https://") or url.startswith("http://")):
        url = "http://"+url
    
    make_html(key, url)
    return key
    

    
def make_html(key: str, url: str):
    target = open(filedir+key+".html", 'w', encoding = 'utf-8')
    target.write(html_body.format(url))
    target.close()
