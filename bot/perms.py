# -*_ coding: utf-8 -*-
#구룩이봇 퍼미션 모듈
import random
import string
from datetime import datetime
import ReadEngine

username_delimeter = '━'

permissions_config = {}
permissions_user = {}
permissions_cmd = {}

permissions_list = ('operator', 'admin', 'nonuser', 'user')

def load_permissions():
    global permissions_config
    global permissions_user
    global permissions_cmd

    permissions_config = ReadEngine.Read_To_Diction("./config/permissions_config.txt")
    permissions_user = ReadEngine.Read_To_Diction("./db/permissions_user.txt")
    permissions_cmd = ReadEngine.Read_To_Diction("./db/permissions_cmd.txt")

def permissions_user_edit(uid: str, username: str, permission: str):
    global permissions_user
    uid = str(uid)
    if(uid in permissions_user.keys()):
        perms = permissions_user[uid].split(username_delimeter)
        if(perms[1] == 'operator'):
            return -2

    if(permission == 'user'):
        try:
            permissions_user.pop(uid)
        except:
            return 1
        else:
            ReadEngine.Write_Diction_To_Text("./db/permissions_user.txt", permissions_user)
            permissions_reload()
            return 1
    
    permstr = str(username) + username_delimeter + permission
    permissions_user[uid] = permstr
    ReadEngine.Write_Diction_To_Text("./db/permissions_user.txt", permissions_user)
    permissions_reload()
    return 1

def permissions_cmd_edit(cid: str, permission: str):
    global permissions_cmd
    if(cid in permissions_cmd.keys()):
        if(permissions_cmd[cid] == 'operator'):
            return -2

    if(permissions_list.index(permission) == 0):
        return -1

    if(permission == 'user'):
        try:
            permissions_cmd.pop(cid)
        except:
            return 1
        else:
            ReadEngine.Write_Diction_To_Text("./db/permissions_cmd.txt", permissions_cmd)
            permissions_reload()
            return 1

    permissions_cmd[cid] = permission
    ReadEngine.Write_Diction_To_Text("./db/permissions_cmd.txt", permissions_cmd)
    permissions_reload()
    return 1

def permissions_manager(uid: str, cid: str):
    global permissions_user
    global permissions_cmd
    uid = str(uid)

    if((uid in permissions_user.keys()) == False):
        user_perm = 'user'
    else:
        user_data = permissions_user[uid].split(username_delimeter)
        user_perm = user_data[1]

    if((cid in permissions_cmd.keys()) == False):
        cmd_perm = 'user'
    else:
        cmd_perm = permissions_cmd[cid]

    if (user_perm == 'nonuser'):
        return -1
    elif (user_perm == 'operator'):
        return 1
    elif (user_perm == 'admin' and (cmd_perm == 'admin' or cmd_perm == 'user')):
        return 1
    elif (user_perm == 'user' and cmd_perm == 'user'):
        return 1
    else:
        return -1

def scramble_op_password():
    global permissions_config
    random.seed(datetime.now())
    pw = "".join([random.choice(string.ascii_letters) for _ in range(8)])
    permissions_config['operator_password'] = pw

    ReadEngine.Write_Diction_To_Text("./config/permissions_config.txt", permissions_config)
    permissions_reload()

def set_admin_password(password: str):
    global permissions_config
    permissions_config['admin_password'] = password

    ReadEngine.Write_Diction_To_Text("./config/permissions_config.txt", permissions_config)
    permissions_reload()

def permissions_reload():
    global permissions_config
    global permissions_user
    global permissions_cmd

    permissions_cmd.clear()
    permissions_config.clear()
    permissions_user.clear()
    load_permissions()



load_permissions()