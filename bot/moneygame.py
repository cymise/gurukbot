# -*_ coding: utf-8 -*-
#구룩이봇 돈기능 확장

#import ReadEngine as RD
from time import time
import datetime
import random
import sqlite3 as sq
import string

member_list = []

class CoinAccount:
    #클래스변수
    minus_limit = -400
    freeze_limit = -200
    instance_list = []#모든 인스턴스 오브젝트가 저장되는 곳!
    uid_list = [] #클래스의 인스턴스의 uid 저장.
    gpu_perfomlist = [(0, 10, 0, 200, 70), (5, 50, 0, 300, 72), (10, 70, 0, 400, 74), (15, 90, 0, 500, 76), (20, 150, 0, 600, 78), (-200, 800, 0, 700, 96), (-2000, 962000, 0, 0, 96)] #그래픽 카드 성능
    #최소, 최대 채굴량, 최소 쿨타임, 최대 쿨타임, 최대 온도( 미구현 )
    gpu_info = [("인텔 내장 그래픽", "0~10", 0), ("GTX 970", "5~50", 500), ("GTX 1080", "10~70", 8000), ("RTX 2080", "10~90", 16000), ("RTX 3090", "20~150", 32000), ("BiRD9620+++", "-200~800", 96200), ("BiRD96962TiSuper", "-2000~962000", 962)]
    gpu_max_grade = len(gpu_info)-1
    def __init__(self, uid, balance, gpu):
        self.uid = uid
        CoinAccount.uid_list.append(self.uid)
        CoinAccount.instance_list.append(self) #인스턴스 스스로를 클래스 변수의 instance_list에 저장
        self.balance = balance
        self.gpu = gpu
        if (self.balance < CoinAccount.freeze_limit):
            self.isFreezed = True
        else:
            self.isFreezed = False
        self.cooldown = 0
    
    def save(self):
        conn = sq.connect('./db/gurukbot.db')
        cursor = conn.cursor()
        cursor.execute("""
            INSERT INTO moneygame (uid, balance, gpu)
            VALUES(?, ?, ?) 
            ON CONFLICT(uid) 
            DO UPDATE SET balance = excluded.balance, gpu = excluded.gpu;
        """, (self.uid, self.balance, self.gpu))
        conn.commit() #커밋
        cursor.close()
        conn.close()

    '''UPSERT 방법 (INSERT 시도 후 충돌 시 UPDATE)
    INSERT INTO cat (id, text)
    VALUES('steven', '52') 
    ON CONFLICT(id) 
    DO UPDATE SET text=excluded.text;
    '''
    def check_info(self):
        return (self.balance, self.gpu, self.isFreezed)

    def add_balance(self, money):
        self.balance = self.balance + money
        if self.balance >= CoinAccount.freeze_limit:
            self.isFreezed = False

        return self.balance

    def sub_balance(self, money):
        if ((self.balance - money) < CoinAccount.minus_limit):
            return 'M'
        elif self.isFreezed:
            return 'F'
        else:
            self.balance  = self.balance - money
        
        if self.balance < CoinAccount.freeze_limit:
            self.isFreezed = True

        return self.balance

    def upgrade_gpu(self):
        if self.gpu == CoinAccount.gpu_max_grade:
            return -2 #더 이상 업그레이드가 안되요.
        else:
            res = self.sub_balance(CoinAccount.gpu_info[self.gpu + 1][2])
            if res == 'M':
                return res
            elif res == 'F':
                return res
            else:
                self.gpu += 1
                return res

    def mine(self):
        random.seed(time())

        if self.cooldown>time():
            return -1
        
        money = random.randint(CoinAccount.gpu_perfomlist[self.gpu][0], CoinAccount.gpu_perfomlist[self.gpu][1])
        cooldown = random.randint(CoinAccount.gpu_perfomlist[self.gpu][2], CoinAccount.gpu_perfomlist[self.gpu][3])
        
        balance = self.add_balance(money)

        if (30 < cooldown < 50):
            self.cooldown = time()+30
            cooldowndate = datetime.datetime.fromtimestamp(self.cooldown).strftime('%Y-%m-%d %H:%M:%S')
            return (-2, money, balance, cooldowndate)
        else:
            return (1, money, balance)

    @classmethod
    def save_all(cls):
        for ins in CoinAccount.instance_list:
            ins.save()


#모듈의 클래스에 속하지 않음에 유의할 것.
def load():
    conn = sq.connect('./db/gurukbot.db')
    cursor = conn.cursor()

    cursor.execute("SELECT uid, balance, gpu FROM moneygame")
    rows = cursor.fetchall()
    for row in rows:
        #rand_name = "".join([random.choice(string.ascii_letters) for _ in range(12)])
        CoinAccount(row[0], row[1], row[2])
        #eval(runstr)
