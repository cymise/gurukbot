# -*_ coding: utf-8 -*-
#구룩이봇 미세먼지, 한강 기능 확장(dusthan)
import requests
import random
import string
from datetime import datetime
import json
import xml.etree.ElementTree

city_airkorea = ('서울', '부산', '대구', '인천', '광주', '대전', '울산', '경기', '강원', '충북', '충남', '전북', '전남', '경북', '경남', '제주', '세종')#바꾸지 마시오.


def dust(city, token):
    if(city_airkorea.count(city) == 0):
        return '-1'

    dust_raw = requests.get(url = 'http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getCtprvnRltmMesureDnsty?sidoName={0}&pageNo=1&numOfRows=10&ServiceKey={1}&ver=1.3'.format(city, token))
    status = dust_raw.status_code

    if (status != 200):
        return '-2'

    dust_xml = xml.etree.ElementTree.fromstring(dust_raw.text)
    
    api_result = dust_xml.find('header').findtext('resultCode')
    if (api_result != '00'):
        return '-3'

    dust_station = []; dust_pm10 = []; dust_pm25 = []; dust_pm10_24h = []; dust_pm25_24h = []; dust_grade_pm10 = []; dust_grade_pm25 = []; dust_data_time = []

    for stations in dust_xml.iter("stationName"):
        dust_station.append(stations.text)

    for pm10 in dust_xml.iter("pm10Value"):
        dust_pm10.append(pm10.text)
    
    for pm25 in dust_xml.iter("pm25Value"):
        dust_pm25.append(pm25.text)

    for pm10 in dust_xml.iter("pm10Value24"):
        dust_pm10_24h.append(pm10.text)
    
    for pm25 in dust_xml.iter("pm25Value24"):
        dust_pm25_24h.append(pm25.text)

    for grade_pm10 in dust_xml.iter("pm10Grade"):
        dust_grade_pm10.append(grade_pm10.text)

    for grade_pm25 in dust_xml.iter("pm25Grade"):
        dust_grade_pm25.append(grade_pm25.text)

    for data_time in dust_xml.iter("dataTime"):
        dust_data_time.append(data_time.text)



    dust_grade_pm10_str = ''
    dust_grade_pm25_str = ''
    dust_result_str = ''
    dust_grade_who_pm10_str = ''
    dust_grade_who_pm25_str = ''

    
    for i in range(0, 3, 1):

        if (dust_grade_pm10[i] == '4'):
            dust_grade_pm10_str = 'PM10 한국: 매우나쁨'
        elif (dust_grade_pm10[i] == '3'):
            dust_grade_pm10_str = 'PM10 한국: 나쁨'
        elif (dust_grade_pm10[i] == '2'):
            dust_grade_pm10_str = 'PM10 한국: 보통'
        elif (dust_grade_pm10[i] == '1'):
            dust_grade_pm10_str = 'PM10 한국: 좋음'
        else:
            dust_grade_pm10_str = 'PM10 한국: 자료없음'

        if (dust_grade_pm25[i] == '4'):
            dust_grade_pm25_str = 'PM2.5 한국: 매우나쁨'
        elif (dust_grade_pm25[i] == '3'):
            dust_grade_pm25_str = 'PM2.5 한국: 나쁨'
        elif (dust_grade_pm25[i] == '2'):
            dust_grade_pm25_str = 'PM2.5 한국: 보통'
        elif (dust_grade_pm25[i] == '1'):
            dust_grade_pm25_str = 'PM2.5 한국: 좋음'
        else:
            dust_grade_pm25_str = 'PM2.5 한국: 자료없음'

        if (not(dust_pm10_24h[i].isnumeric())):
            dust_grade_who_pm10_str = 'PM10 WHO: 자료없음'
        elif (int(dust_pm10_24h[i]) > 150):
            dust_grade_who_pm10_str = 'PM10 WHO: 최악'
        elif (int(dust_pm10_24h[i]) > 100):
            dust_grade_who_pm10_str = 'PM10 WHO: 매우나쁨'
        elif (int(dust_pm10_24h[i]) > 75):
            dust_grade_who_pm10_str = 'PM10 WHO: 상당히나쁨'
        elif (int(dust_pm10_24h[i]) > 50):
            dust_grade_who_pm10_str = 'PM10 WHO: 나쁨'
        elif (int(dust_pm10_24h[i]) > 40):
            dust_grade_who_pm10_str = 'PM10 WHO: 보통'
        elif (int(dust_pm10_24h[i]) > 30):
            dust_grade_who_pm10_str = 'PM10 WHO: 양호'
        elif (int(dust_pm10_24h[i]) > 15):
            dust_grade_who_pm10_str = 'PM10 WHO: 좋음'
        else:
            dust_grade_who_pm10_str = 'PM10 WHO: 매우좋음'

        if (not(dust_pm25_24h[i].isnumeric())):
            dust_grade_who_pm25_str = 'PM2.5 WHO: 자료없음'
        elif (int(dust_pm25_24h[i]) > 75):
            dust_grade_who_pm25_str = 'PM2.5 WHO: 최악'
        elif (int(dust_pm25_24h[i]) > 50):
            dust_grade_who_pm25_str = 'PM2.5 WHO: 매우나쁨'
        elif (int(dust_pm25_24h[i]) > 37):
            dust_grade_who_pm25_str = 'PM2.5 WHO: 상당히나쁨'
        elif (int(dust_pm25_24h[i]) > 25):
            dust_grade_who_pm25_str = 'PM2.5 WHO: 나쁨'
        elif (int(dust_pm25_24h[i]) > 20):
            dust_grade_who_pm25_str = 'PM2.5 WHO: 보통'
        elif (int(dust_pm25_24h[i]) > 15):
            dust_grade_who_pm25_str = 'PM2.5 WHO: 양호'
        elif (int(dust_pm25_24h[i]) > 8):
            dust_grade_who_pm25_str = 'PM2.5 WHO: 좋음'
        else:
            dust_grade_who_pm25_str = 'PM2.5 WHO: 매우좋음'
            
    
        dust_result_str = dust_result_str + '**<' +dust_station[i] + '관측소>**' + '\n' + '시각: ' + dust_data_time[i] + '\n' + 'PM10: ' + dust_pm10[i] + ' ㎍/㎥' + ', ' + 'PM2.5: ' + dust_pm25[i] + ' ㎍/㎥'+ '\n'+ dust_grade_pm10_str +', ' +dust_grade_pm25_str + '\n' + dust_grade_who_pm10_str + ', ' + dust_grade_who_pm25_str +'\n\n'

    return dust_result_str


def hangang():
    url = 'http://hangang.dkserver.wo.tc/'
    response = requests.get(url = url)
    status = response.status_code
    data = json.loads(response.text)

    if (status == 200):
        return data["temp"]#온도는 문자열 형식임
    else :
        return 'f'
