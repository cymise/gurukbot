# -*_ coding: utf-8 -*-

#구룩이봇 기록삭제, 키워드삭제 기능 (미사용)
import discord
from discord.ext import commands
import asyncio
import ReadEngine
from datetime import datetime

tokens = {}

def load_config():
    global tokens
    tokens = ReadEngine.Read_To_Diction("./config/general_config.txt")

load_config()
prefix = tokens['prefix']
bot = commands.Bot(command_prefix="+")

@bot.event
async def on_ready():
    print('구룩이봇 로그인')
    print(bot.user.name)
    print(bot.user.id)

@bot.command(pass_context = True)
async def 기록삭제(ctx, name: discord.Member):
    user = ctx.author
    date = datetime.now
    count = 0

    async for message in ctx.channel.history(limit = None, oldest_first = None):
        if (message.author.id == name.id):
            await message.delete()
            count = count + 1
            if((count%100) == 0 and count>1):
                await ctx.send("현재 채널에서 메시지{0}개를 최신순으로 삭제함.".format(count))

    await ctx.send("현재 채널에서 메시지{0}개를 최신순으로 삭제함.".format(count))

@bot.command(pass_context = True)
async def 기록삭제2(ctx, name: discord.Member):
    user = ctx.author
    date = datetime.now
    count = 0

    async for message in ctx.channel.history(limit = None, oldest_first = True):
        if (message.author.id == name.id):
            await message.delete()
            count = count + 1
            if((count%100) == 0 and count>1):
                await ctx.send("현재 채널에서 메시지{0}개를 오래된순으로 삭제함.".format(count))

    await ctx.send("현재 채널에서 메시지{0}개를 오래된순으로 삭제함.".format(count))
    
            
bot.run(tokens['bot_token'])