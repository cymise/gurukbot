# -*_ coding: utf-8 -*-
#구룩이봇 조류백과 키워드 모듈 (BirdEncyclopedia)

import ReadEngine

custom_birds_diction = {}

def load_birds():
    global custom_birds_diction
    custom_birds_diction = ReadEngine.Read_To_Diction('./db/birds_list.txt')

def write_birds(cmd, con):
    ReadEngine.Add_To_Text('./db/birds_list.txt', cmd, con)

def delete_birds(cmd):
    global custom_birds_diction

    try:
        custom_birds_diction.pop(cmd)
    except:
        return -1
    else:
        ReadEngine.Write_Diction_To_Text('./db/birds_list.txt', custom_birds_diction)
        return 0

def reload_birds():
    global custom_birds_diction
    custom_birds_diction.clear()
    load_birds()


load_birds()