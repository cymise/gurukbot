import openai

# Set your OpenAI API key
openai.api_key = "YOUR_API_KEY"

max_prompt_length = 30
prompt = []

def load(apikey):
    openai.api_key = apikey

def text_response(input_text):
    global prompt
    global max_prompt_length

    prompt.append("USER: " + str(input_text))
    if len(prompt) > max_prompt_length:
        prompt = prompt[len(prompt) - max_prompt_length:]

    context = '\n'.join(prompt)  # Join the prompt list into a single string
    
    response = openai.ChatCompletion.create(
        model="gpt-4o",
        messages=[
            {"role": "system", "content": context},
            {"role": "user", "content": input_text}
        ],
    )

    response_txt = response.choices[0].message["content"].strip()  # Strip whitespace from response
    prompt.append("AI: " + response_txt)

    return response_txt


def text_response_35(input_text):
    global prompt
    global max_prompt_length

    prompt.append("USER: " + str(input_text))
    if len(prompt) > max_prompt_length:
        prompt = prompt[len(prompt) - max_prompt_length:]

    context = '\n'.join(prompt)  # Join the prompt list into a single string
    
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": context},
            {"role": "user", "content": input_text}
        ],
    )

    response_txt = response.choices[0].message["content"].strip()  # Strip whitespace from response
    prompt.append("AI: " + response_txt)

    return response_txt