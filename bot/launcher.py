# -*_ coding: utf-8 -*-
import discord
from discord.ext import commands
import asyncio
import sys
import os
import string

import ReadEngine
import perms

tokens = {}

def load_config():
    global tokens
    tokens = ReadEngine.Read_To_Diction("./config/general_config.txt")

##########################################################
load_config()
prefix = tokens['prefix']
bot = commands.Bot(command_prefix=prefix)
bot.remove_command("help")
##########################################################

@bot.event
async def on_ready():
    print('봇이 로그인했습니다. 그러나 봇 코어는 아직 실행되지 않았습니다.')
    print("이름: ", bot.user.name)
    print("UID: ", bot.user.id)

    presence = discord.Game("봇이 아직 실행되지 않았습니다. {0}start 로 시작하세요.".format(prefix))
    await bot.change_presence(activity=presence)


@bot.command(pass_context = True)
async def start(ctx, cmd = ""):
    """봇을 시작합니다."""
    cid = 'bot.start'
    uid = ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send("당신은 봇을 실행할 권한이 없습니다. admin 또는 operator 권한 수준을 가진 사용자가 실행 가능합니다.")
        return

    await ctx.send("봇을 시작합니다. (약간의 시간이 소요될 수 있습니다.)")
    #rtrn = os.system('python3.7 bot.py')
    rtrn = os.system('/usr/bin/python3 bot.py')
    if (rtrn != 0):
        await ctx.send("봇 실행이 정상적이지 않았습니다. 문제점을 확인해주세요.")

    presence = discord.Game("봇이 아직 실행되지 않았습니다. {0}start 로 시작하세요.".format(prefix))
    await bot.change_presence(activity=presence)

@bot.command(pass_context = True)
async def update(ctx, cmd = ""):
    """봇을 업데이트합니다."""
    cid = 'bot.update'
    uid = ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send("당신은 봇을 업데이트할 권한이 없습니다. admin 또는 operator 권한 수준을 가진 사용자가 실행 가능합니다.")
        return
    
    await ctx.send("이 과정은 사용자의 설정 파일까지 업데이트하지 않습니다. 봇 런처의 변경사항은 수동으로 재시작 해야 적용됩니다.")
    await ctx.send("정보를 받는 중입니다. (약간의 시간이 소요될 수 있습니다.)")
    os.system('git clone https://gitlab.com/cymise/gurukbot ./temp')
    await ctx.send("파일을 복사합니다.")
    os.system('cp -f ./temp/bot/*.py ./')
    os.system('rm -rf ./temp')
    await ctx.send("업데이트를 완료했습니다.")

@bot.command(pass_context = True)
async def exit(ctx, cmd = ""):
    """봇을 완전히 종료합니다."""
    cid = 'bot.shutdown'
    uid = ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send("당신은 봇을 종료할 권한이 없습니다. admin 또는 operator 권한 수준을 가진 사용자가 실행 가능합니다.")
        return

    await ctx.send("Goodbye.")
    sys.exit(0)
    


bot.run(tokens['bot_token'])