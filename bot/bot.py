# -*_ coding: utf-8 -*-
#구룩이봇 메인
import discord
from discord.ext import commands
import requests
import asyncio
import random
import string
from datetime import datetime
import json
import xml.etree.ElementTree
import sys
import platform
import imp

#-----아래는 구룩이봇의 기능 필수 구성요소입니다.-----#
import ReadEngine
import CommandEngine
import perms
import helper
import dusthan
import silverhair
#import guruktube
#import translator
import moneygame
import gurukchat
#import urlshort

tokens = {}
city_airkorea = ('서울', '부산', '대구', '인천', '광주', '대전', '울산', '경기', '강원', '충북', '충남', '전북', '전남', '경북', '경남', '제주', '세종')#바꾸지 마시오.
#img_ext = ("jpg", "png", "jpeg", "gif", "JPG", "JPEG", "PNG", "GIF")
#url_head = ("http://", "https://")

admin_glob = []
forbidden_glob = []
permissions = {}
password_glob = []

perm_error = "⚠️ 권한이 부족해요.\n당신이 관리자에 의해 차단되었거나, 중요한 명령을 실행하려고 하는 것 같아요."
version = "구룩이봇 - 2302.01 (23.02.27)"

def embeder(title = "", url = "", desc = "", image = "", color = 0x3962e0, footer = ""):
    embed = discord.Embed(title = title, url = url, description = desc, color = color)
    embed.set_footer(text = footer)
    if (image !=""):
        embed.set_image(url=image)
    return embed


def load_config():
    global tokens
    tokens = ReadEngine.Read_To_Diction("./config/general_config.txt")



##########################################################
load_config()
prefix = tokens['prefix']
bot = commands.Bot(command_prefix=prefix)
bot.remove_command("help")
CommandEngine.load_custom_commands()
CommandEngine.load_custom_keywords()
moneygame.load()
gurukchat.load(tokens['openai_token'])
##########################################################

@bot.event
async def on_ready():
    print('봇이 로그인했습니다.')
    print("이름: ", bot.user.name)
    print("UID: ", bot.user.id)

    presence = discord.Game("도움말은 {0}도움말을 입력하면 볼 수 있어요.".format(prefix))
    await bot.change_presence(activity=presence)

@bot.command(pass_context = True)
async def 도움말(ctx, cmd = ""):
    """명령어 목록을 표시합니다."""
    cid = '도움말'
    uid = ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return
    
    if(cmd == ""):
        description = helper.help_str()
    else:
        description = helper.cmd_help(cmd)
    
    embed = discord.Embed(title="도움말", description=description, color=0x3962e0)
    await ctx.send(embed = embed)



@bot.command(pass_context = True)
async def 정보(ctx):
    """봇의 정보를 표시합니다."""

    uid= ctx.author.id
    cid = '정보'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return


    embed = discord.Embed(title="구룩이 봇 정보", url="https://gitlab.com/cymise/gurukbot", description="🐦구룩이 봇🐦\n버전: {0}\nPython 버전: {1}\n플랫폼: {2}, {3}\n\n미세먼지정보: Air Korea Open API (공공데이터포털)\nCC BY\n\n구룩이봇은 BSD 라이선스를 따릅니다.\n".format(version, sys.version, platform.platform(), platform.machine()), color=0x3962e0)
    embed.set_footer(text="Iris (theonebird81@gmail.com)\n")
    embed.set_image(url="https://cdn.discordapp.com/attachments/685259385005408286/719163281620598854/03b51cf87035bef4.png")
    await ctx.send(embed = embed)

@bot.command(pass_context = True)
async def neko(ctx):
    """세이프부루에서 랜덤한 아니메 고양이 소녀 짤을 가져옵니다."""
    cid = 'neko'
    uid= ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    tag = "cat_girl"
    (imglink, postid) = silverhair.search(tag)
    safebooru_url = "https://safebooru.org/index.php?page=post&s=view&id=" + postid
    embed = embeder(title = "Safebooru", url = safebooru_url, desc = "", image = imglink, color = 0x3962e0, footer = "neko")
    await ctx.send(embed = embed)

@bot.command(pass_context = True)
async def 야옹(ctx):
    """https://thecatapi.com 에서 고양이 짤을 가져옵니다."""
    cid = '야옹'
    uid= ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    embed = embeder(title = "TheCatAPI", url = "https://thecatapi.com", desc = "", image = silverhair.cat(), color = 0x3962e0, footer = "야옹")
    await ctx.send(embed = embed)

@bot.command(pass_context = True)
async def 은발(ctx):
    """세이프부루에서 은발 짤을 가져옵니다."""
    cid = '은발'
    uid= ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return


    tag = "silver_hair"
    (imglink, postid) = silverhair.search(tag)
    safebooru_url = "https://safebooru.org/index.php?page=post&s=view&id=" + postid
    embed = embeder(title = "Safebooru", url = safebooru_url, desc = "", image = imglink, color = 0x3962e0, footer = "은발")
    await ctx.send(embed = embed)



@bot.command(pass_context = True)
async def img(ctx, tag: str):
    """세이프부루에서 원하는 일러스트를 가져옵니다."""
    cid = 'img'
    uid= ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    (imglink, postid) = silverhair.search(tag)
    safebooru_url = "https://safebooru.org/index.php?page=post&s=view&id=" + postid
    embed = embeder(title = "Safebooru", url = safebooru_url, desc = "", image = imglink, color = 0x3962e0, footer = "짤검색")
    await ctx.send(embed = embed)


@bot.command(pass_context = True)
async def vs(ctx, left : str, right: str):
    """누가 더 나을까요?"""

    uid= ctx.author.id
    cid = 'vs'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    choice1 = random.randint(0, 4)
    choice2 = random.randint(0, 1)
    if (left == "쯔양"):
        await ctx.send('{0}이(가) 훨씬 낫네요.'.format(right))
        
    elif (right == "쯔양"):
        await ctx.send('{0}이(가) 훨씬 낫네요.'.format(left))

    elif ((left == '돌멩이' or left == '돌맹이' or left == '돌' or left == '자수정') or (right == '돌멩이' or right == '돌맹이' or right == '돌' or right == '자수정')):
        if(choice1 == 0):
            await ctx.send('돌멩이는 제 주인입니다..')
        if(choice1 == 1):
            await ctx.send('돌멩이가 최고같군요.')
        if(choice1 == 2):
            await ctx.send('이래뵈도 자수정은 보석류에 속합니다만?')
        if(choice1 == 3):
            await ctx.send('돌이 더 나아보입니다.')

    else:
        if (choice1 == 0):
            if (choice2 == 0):
                await ctx.send('{0}이(가) 더 낫네요.'.format(left))
            else :
                await ctx.send('{0}이(가) 더 낫네요.'.format(right))

        if (choice1 == 1):
            if (choice2 == 0):
                await ctx.send('비교할 가치가 없네요. 둘 다 똥이군요.')
            else :
                await ctx.send('미쳤습니까 HUMAN?')
                await ctx.send('https://grgi.ga/bigdata.png')

        if (choice1 == 2):
            if (choice2 == 0):
                await ctx.send('{0}이(가) 훨씬 낫네요.'.format(left))
            else :
                await ctx.send('{0}이(가) 훨씬 낫네요.'.format(right))

        if (choice1 == 3):
            if (choice2 == 0):
                await ctx.send('잘 모르겠어요. 그냥 아무거나 하세요.')
            else :
                await ctx.send('귀찮네요. 아무거나 하세요. ')

            '''
            url = 'http://hangang.dkserver.wo.tc/'
            response = requests.get(url = url)
            status = response.status_code
            data = json.loads(response.text)

            if (status == 200):
                await ctx.send('저걸 비교할 바에는 차라리 자살하러 갑니다. 지금 한강 물 온도는 {0}도네요.'.format(data['temp']))
            else :
                await ctx.send('저걸 비교할 바에는 자살하러 갑니다. 지금 한강 물 온도가 몇도인지는 모르겠지만.')
            '''

        if (choice1 == 4):
            if (choice2 == 0):
                await ctx.send('{0}이(가) {1}보다는 좋아보이네요.'.format(left, right))
            else :
                await ctx.send('{0}이(가) {1}보다는 좋아보이네요.'.format(right, left))

#@bot.command(pass_context=True)
#async def 한강(ctx):
    #"""한강 수온을 보여줍니다."""

    #uid= ctx.author.id
    #cid = '한강'

    #if (perms.permissions_manager(uid, cid) == -1):
    #    await ctx.send(perm_error)
    #    return
    
    #await ctx.send('⚠️ 한강 수온은 더 이상 지원하지 않습니다.\n누가 그랬냐구요? 돌멩이한테 가서 따지세요.')
    #return

    #result = dusthan.hangang()
    #if (result == 'f'):
    #    await ctx.send("한강 수온을 불러오지 못했습니다.")
    #else:
    #    await ctx.send("지금 한강 수온은 섭씨 {0}도 입니다.".format(result))

@bot.command(pass_context=True)
async def 미세먼지(ctx, city = 'default'):
    """제발 좀 없어져라."""

    uid= ctx.author.id
    cid = '미세먼지'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    result_str = dusthan.dust(city, tokens["airkorea_token"])

    if result_str == '-1':
        await ctx.send('⚠️ 입력한 지역은 유효하지 않습니다. `서울, 부산, 대구, 인천, 광주, 대전, 울산, 경기, 강원, 충북, 충남, 전북, 전남, 경북, 경남, 제주, 세종` 중 하나만 입력하세요.')
    elif result_str == '-2':
        await ctx.send('⚠️ 정보를 받아올 수 없습니다.')
    elif result_str == '-3':
        await ctx.send('⚠️ 정보를 받아올 수 없습니다. 공공데이터 포털 API 키가 잘못되었을 것입니다. 봇 관리자에게 문의하십시오.')
    else:
        embed = embeder('{0} 지역 미세먼지'.format(city), "", result_str, "", 0x3962e0, 'by 공공데이터포털, 에어코리아. CC-BY')
        await ctx.send(embed = embed)

@bot.command(pass_context = True)
async def 명령추가(ctx):

    uid= ctx.author.id
    cid = '명령추가'
    

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[6:]
    
    if (msg_str[0] == '"'):
        start = msg_str.find('"') + 1
        end = msg_str.find('"', 1)
        left = msg_str[start:end]
        right = msg_str[(end+1):]
    else:
        start = 0
        end = msg_str.find(" ")
        left = msg_str[start:end]
        right = msg_str[(end+1):]


#    if (left in CommandEngine.custom_commands_diction.keys() and not(overwrite == '네' or overwrite == 'y' or overwrite == 'Y')):
#        await ctx.send('⚠️ 명령어를 추가할 수 없습니다. 이미 그 명령어가 존재합니다.\n기존 명령어: `{0}: {1}`\n덮어쓰기 하려면 명령어 끝에 `y` 혹은 `네` 를 쓰십시오.'.format(left, CommandEngine.custom_commands_diction[left]))
#        return

    CommandEngine.write_custom_commands(left, right)
    CommandEngine.reload_custom_commands()
    await ctx.send('✅ 명령어 `{0}: {1}` 를 추가했어요.'.format(left, right))


@bot.command(pass_context = True)
async def 명령삭제(ctx, cmd: str):
    uid= ctx.author.id
    cid = '명령삭제'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[6:]

    result = CommandEngine.delete_custom_commands(msg_str)
    if result == -1:
        await ctx.send('⚠️ 명령어 `{0}`는 없어요.'.format(msg_str))
    else:
        await ctx.send('✅ 명령어 `{0}`이(가) 삭제되었어요.'.format(msg_str))

@bot.command(pass_context = True)
async def 명령목록(ctx, page = 1):
    uid= ctx.author.id
    cid = '명령목록'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    command_key = list(CommandEngine.custom_commands_diction.keys())
    command_content = list(CommandEngine.custom_commands_diction.values())
    page_str = '✅ 명령어 수: {0}개\n**<명령어 : 내용>**\n'.format(len(CommandEngine.custom_commands_diction))

    max_pages = (len(command_key) // 15)
    if (max_pages * 15 < len(command_key) ):
        max_pages += 1

    if (page > max_pages):
        page = max_pages
    elif (page < 1):
        page = 1

    for i in range(15 * (page - 1), (15 * page), 1):
        if (i >= len(command_key)):
            break
        page_str = page_str + command_key[i] + ' : ' + command_content[i] +'\n'

    if (len(page_str)>2000):
        await ctx.send("내용이 너무 길어 생략되었어요.")
        page_str = '✅ 명령어 수: {0}개\n**<명령어 : 내용>**\n'.format(len(CommandEngine.custom_commands_diction))

        for i in range(15 * (page - 1), (15 * page), 1):
            if (i >= len(command_key)):
                break
            page_str = page_str + command_key[i] + '\n'


    embed = embeder("사용자 명령 목록", "", page_str, "", 0x3962e0, "<{0} 페이지 중 {1} 페이지>".format(max_pages, page))
    await ctx.send(embed = embed)


@bot.command(pass_context = True)
async def 키워드추가(ctx):

    uid= ctx.author.id
    cid = '키워드추가'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[7:]
    
    if (msg_str[0] == '"'):
        start = msg_str.find('"') + 1
        end = msg_str.find('"', 1)
        left = msg_str[start:end]
        right = msg_str[(end+1):]
    else:
        start = 0
        end = msg_str.find(" ")
        left = msg_str[start:end]
        right = msg_str[(end+1):]

#    if (left in CommandEngine.custom_keywords_diction.keys() and not(overwrite == '네' or overwrite == 'y' or overwrite == 'Y')):
#        await ctx.send('⚠️ 키워드를 추가할 수 없습니다. 이미 그 키워드가 존재합니다.\n기존 키워드: `{0}: {1}`\n덮어쓰기 하려면 키워드 끝에 `y` 혹은 `네` 를 쓰십시오.'.format(left, CommandEngine.custom_keywords_diction[left]))
#        return

    CommandEngine.write_custom_keywords(left, right)
    CommandEngine.reload_custom_keywords()
    await ctx.send('✅ 키워드 `{0}: {1}` 를 추가했어요.'.format(left, right))


@bot.command(pass_context = True)
async def 키워드삭제(ctx, cmd: str):
    uid= ctx.author.id
    cid = '키워드삭제'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[7:]

    result = CommandEngine.delete_custom_keywords(msg_str)
    if result == -1:
        await ctx.send('⚠️ 키워드 `{0}`는 없어요.'.format(msg_str))
    else:
        await ctx.send('✅ 키워드 `{0}`이(가) 삭제되었어요.'.format(msg_str))

@bot.command(pass_context = True)
async def 키워드목록(ctx, page = 1):
    uid= ctx.author.id
    cid = '키워드목록'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    keyword_key = list(CommandEngine.custom_keywords_diction.keys())
    keyword_content = list(CommandEngine.custom_keywords_diction.values())
    page_str = '✅ 키워드 수: {0}개\n**<키워드 : 내용>**\n'.format(len(CommandEngine.custom_keywords_diction))

    max_pages = (len(keyword_key) // 15)
    if (max_pages * 15 < len(keyword_key) ):
        max_pages += 1

    if (page > max_pages):
        page = max_pages
    elif (page < 1):
        page = 1

    for i in range(15 * (page - 1), (15 * page), 1):
        if (i >= len(keyword_key)):
            break

        page_str = page_str + keyword_key[i] + ' : ' + keyword_content[i] +'\n'

    if (len(page_str)>2000):
        await ctx.send("내용이 너무 길어 생략되었어요.")
        page_str = '✅ 키워드 수: {0}개\n**<키워드 : 내용>**\n'.format(len(CommandEngine.custom_keywords_diction))

        for i in range(15 * (page - 1), (15 * page), 1):
            if (i >= len(keyword_key)):
                break
            page_str = page_str + keyword_key[i] + '\n'

    embed = embeder("사용자 키워드 목록", "", page_str, "", 0x3962e0, "<{0} 페이지 중 {1} 페이지>".format(max_pages, page))
    await ctx.send(embed = embed)

#-----------------------------------------
@bot.command(pass_context = True)
async def 태그추가(ctx):

    uid= ctx.author.id
    cid = 'img'
    

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[6:]
    
    if (msg_str[0] == '"'):
        start = msg_str.find('"') + 1
        end = msg_str.find('"', 1)
        left = msg_str[start:end]
        right = msg_str[(end+1):]
    else:
        start = 0
        end = msg_str.find(" ")
        left = msg_str[start:end]
        right = msg_str[(end+1):]



    CommandEngine.write_custom_imgtag(left, right)
    CommandEngine.reload_custom_imgtag()
    await ctx.send('✅ 사용자 이미지태그 `{0}: {1}`를 추가했어요.'.format(left, right))


@bot.command(pass_context = True)
async def 태그삭제(ctx, cmd: str):
    uid= ctx.author.id
    cid = 'img'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[6:]

    result = CommandEngine.delete_custom_imgtag(msg_str)
    if result == -1:
        await ctx.send('⚠️ 사용자 이미지태그 `{0}`는 없어요.'.format(msg_str))
    else:
        await ctx.send('✅ 사용자 이미지태그 `{0}`이(가) 삭제되었어요.'.format(msg_str))

@bot.command(pass_context = True)
async def 태그목록(ctx, page = 1):
    uid= ctx.author.id
    cid = 'img'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    command_key = list(CommandEngine.custom_imgtag_diction.keys())
    command_content = list(CommandEngine.custom_imgtag_diction.values())
    page_str = '✅ 명령어 수: {0}개\n**<태그 : 내용>**\n'.format(len(CommandEngine.custom_imgtag_diction))

    max_pages = (len(command_key) // 15)
    if (max_pages * 15 < len(command_key) ):
        max_pages += 1

    if (page > max_pages):
        page = max_pages
    elif (page < 1):
        page = 1

    for i in range(15 * (page - 1), (15 * page), 1):
        if (i >= len(command_key)):
            break
        page_str = page_str + "`" + command_key[i] + ' : ' + command_content[i] + "`" +'\n'

    if (len(page_str)>2000):
        await ctx.send("내용이 너무 길어 생략되었어요.")
        page_str = '✅ 명령어 수: {0}개\n**<태그 : 내용>**\n'.format(len(CommandEngine.custom_imgtag_diction))

        for i in range(15 * (page - 1), (15 * page), 1):
            if (i >= len(command_key)):
                break
            page_str = page_str + command_key[i] + '\n'


    embed = embeder("사용자 태그 목록", "", page_str, "", 0x3962e0, "<{0} 페이지 중 {1} 페이지>".format(max_pages, page))
    await ctx.send(embed = embed)


@bot.command(pass_context = True)
async def c(ctx, cmd: str):
    uid= ctx.author.id
    cid = 'chat'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[3:]

    response = gurukchat.text_response(msg_str)
    await ctx.send(response)

@bot.command(pass_context = True)
async def cc(ctx, cmd: str):
    uid= ctx.author.id
    cid = 'chat'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[4:]

    response = gurukchat.text_response_35(msg_str)
    await ctx.send(response)


""" @bot.command(pass_context = True)
async def 유튜브(ctx, left: str, right: str = "보통"):
    uid = ctx.author.id
    cid = '유튜브'
    opt = 2
    opt_string = "비디오:보통화질"

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return
    
    if (right == '보통음질'):
        opt = 0
        opt_string = "음악:보통음질"

    elif (right == "최고음질"):
        opt = 1
        opt_string = "음악:최고음질"

    elif (right == "보통"):
        opt = 2
        opt_string = "비디오:보통화질"

    elif (right == "최고"):
        opt = 3
        opt_string = "비디오:최고화질"
        
    else:
        opt = 2
        opt_string = "비디오:보통화질"

    yturl = guruktube.search(left)
    await ctx.send("유튜브에서 {0}을 {1}로 다운로드 합니다.\n{2}".format(yturl[0], opt_string, yturl[1]))
    dlurl = guruktube.dload(left, opt)
    await ctx.send("다운로드 된 파일의 URL은 다음과 같습니다.\n`{0}`\n{0}".format(dlurl))

@bot.command(pass_context = True)
async def 유튜브삭제(ctx):
    uid = ctx.author.id
    cid = '유튜브삭제'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    guruktube.clean()
    await ctx.send("영상 저장폴더의 내용을 삭제했습니다.")

@bot.command(pass_context = True)
async def 단축(ctx, url:str):
    uid = ctx.author.id
    cid = 'url단축'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    key = urlshort.make_short(url)
    await ctx.send("단축된 주소는\nhttps://ghk.one/{0}\n입니다.".format(key)) """

@bot.command(pass_context = True)
async def 삭제기(ctx, pw = ""):
    uid = ctx.author.id
    cid = '삭제기'
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    count = 0
    if (pw == "삭제동의"):
        async for message in ctx.channel.history(limit = None, oldest_first = None):
            if (message.author.id == ctx.author.id):
                await message.delete()
                count = count + 1
                if((count%200) == 0 and count>1):
                    await ctx.send("삭제중: 현재 채널에서 메시지{0}개를 최신순으로 삭제했어요.".format(count))

        await ctx.send("삭제 완료: 작업이 끝났어요. 현재 채널에서 총 메시지{0}개가 삭제되었어요.".format(count))

    else:
        await ctx.send("⚠️이 명령어는 이 채널에 있는 당신의 모든 채팅을 삭제합니다.\n이 작업은 되돌릴 수 없으며, 일단 작업이 시작되면 취소할 수 없습니다.\n진행을 원하시면 `{0}삭제기 삭제동의`명령을 입력하세요.".format(prefix))
    return

@bot.command(pass_context = True)
async def trans(ctx, dest, *args):
    string = ' '.join(args)
    destination = dest

    #print("r_str   :", raw_string, "   destination   :", destination, "   trans:", trans)

    result = translator.translate(string, destination)
    await ctx.send("Translate to {0}: {1}".format(destination, result))

@bot.group(pass_context = True)
async def gc(ctx):
    return

@gc.command(pass_context = True)
async def 구룩코인(ctx):
    info_str_1 = "구룩코인은 **가치 없는** 가상화폐입니다.\n 구룩코인은 다음 기능을 지원합니다.\n\n"
    info_str_2 = "**계좌생성**: 계좌를 만듭니다.\n**내정보**: 나의 계좌 정보를 봅니다.\n**송금** (보낼사람) (금액): 다른 사람에게 구룩코인을 보냅니다.\n**채굴**: 구룩코인을 채굴합니다.\n**업그레이드**: 구룩코인을 채굴할 수 있는 그래픽카드를 업그레이드합니다."
    embed = embeder(title="구룩이 가상화폐 - 구룩코인", desc=info_str_1+info_str_2)
    await ctx.send(embed = embed)

@gc.command(pass_context = True)
async def 계좌생성(ctx):
    #이미 계좌가 있으면 거절하는 로직 만들기
    uid = ctx.author.id
    if uid in moneygame.CoinAccount.uid_list:
        info_str = "{0}님은 이미 계좌가 있어요.".format(ctx.author.mention)
    else:
        #rand_name = "".join([random.choice(string.ascii_letters) for _ in range(12)])
        moneygame.CoinAccount(uid, 100, 0)
        #eval(runstr)
        index = moneygame.CoinAccount.uid_list.index(uid)
        moneygame.CoinAccount.instance_list[index].save()
        info_str = "{0}님의 계좌가 생성되었어요.\n 계좌 생성 기념으로 100GC를 넣었습니다.".format(ctx.author.mention)

    embed = embeder(title="구룩이 가상화폐 - 구룩코인", desc=info_str)
    await ctx.send(embed = embed)

@gc.command(pass_context = True)
async def 내정보(ctx):
    uid = ctx.author.id
    if not(uid in moneygame.CoinAccount.uid_list):
        await ctx.send("{0}님은 계좌가 없어요. 먼저 계좌를 생성하세요.".format(ctx.author.mention))
        return
    else:
        index = moneygame.CoinAccount.uid_list.index(uid)
        (balance, gpu, isFreezed) = moneygame.CoinAccount.instance_list[index].check_info()
        balance_str = str(balance)
        
        gpu_strlist = [ "인텔 내장 그래픽", "GTX 970", "GTX 1080", "RTX 2080", "RTX 3090", "BIRD 9620Ti+++" ]
        gpu_str = gpu_strlist[gpu]

        if isFreezed == True:
            freeze_str = "당신의 계좌는 동결되었어요. 동결을 해제하기 위해 코인을 채굴하거나 돈을 송금받으세요."
        else:
            freeze_str = "현재 계좌는 정상이에요. 한도 내에서 자유롭게 출금 가능해요."

        info_str = "당신의 구룩코인 잔액: " + balance_str + "GC\n\n" + "당신의 GPU 등급: " + gpu_str + "\n\n" + freeze_str
        embed = embeder(title="구룩코인 - 내정보", desc=info_str)
        await ctx.send(embed = embed)


@gc.command(pass_context = True)
async def 송금(ctx, name: discord.Member, amount: int):
    uid = ctx.author.id
    transaction = False
    if amount < 0:
        info_str = "유효하지 않은 거래입니다. \n0보다 작은 금액은 송금할 수 없어요."
        embed = embeder(title="구룩코인 - 송금", desc=info_str)
        await ctx.send(embed = embed)
        return

    if not(uid in moneygame.CoinAccount.uid_list):
        info_str = "{0}님은 계좌가 없어요. 먼저 계좌를 생성하세요.".format(ctx.author.mention)
        embed = embeder(title="구룩코인 - 송금", desc=info_str)
        await ctx.send(embed = embed)

    elif not(name.id in moneygame.CoinAccount.uid_list):
        info_str = "받을 사람의 계좌가 없어서 송금할 수 없어요.\n(받을 사람의 계좌가 있다면, 닉네임을 제대로 쓰셨는지 한번 더 확인해보세요.)"
        embed = embeder(title="구룩코인 - 송금", desc=info_str)
        await ctx.send(embed = embed)

    else:
        index_1 = moneygame.CoinAccount.uid_list.index(uid)
        index_2 = moneygame.CoinAccount.uid_list.index(name.id)
        
        info_str = "{0}님, 정말로 {1}님에게 {2}GC를 보내는 것이 맞나요?\n맞다면 **네**라고 20초 내에 답해주세요.\n취소하고 싶으시다면 **아니오**를 입력하세요.".format(ctx.author.mention, name, amount)
        embed = embeder(title="송금 확인", desc=info_str)
        await ctx.send(embed = embed)

        def check(m):
            nonlocal transaction
            if m.author == ctx.author and (m.content == "네"):
                transaction = True
                return True
            elif m.author == ctx.author and (m.content == "아니오"):
                transaction = False
                return True
            else:
                return False

        try:
            msg = await bot.wait_for('message', check=check, timeout=20.0)
        except asyncio.TimeoutError:
            transaction = False
        
    if transaction == True:
        result = moneygame.CoinAccount.instance_list[index_1].sub_balance(amount)
        if result == 'M':
            info_str = "{0}님, 마이너스 한도인 {1}GC를 초과하기 때문에 송금할 수 없어요.".format(ctx.author.mention, moneygame.CoinAccount.minus_limit)
        elif result == 'F':
            info_str = "{0}님의 계좌는 동결되어 있어요.".format(ctx.author.mention)
        else:
            balance = result
            moneygame.CoinAccount.instance_list[index_2].add_balance(amount)
            moneygame.CoinAccount.instance_list[index_1].save()
            moneygame.CoinAccount.instance_list[index_2].save()
            info_str = "{}님의 계좌에 송금했어요. {}님의 잔액은 {}GC 입니다.".format(name, ctx.author.mention, balance)
        embed = embeder(title="구룩코인 - 송금", desc=info_str)
        await ctx.send(embed = embed)

    elif transaction == False:
        info_str = "{0}님, 거래가 취소되었습니다.".format(ctx.author.mention)
        embed = embeder(title="구룩코인 - 송금", desc=info_str)
        await ctx.send(embed = embed)

@gc.command(pass_context = True)
async def 채굴(ctx):
    uid = ctx.author.id
    if not(uid in moneygame.CoinAccount.uid_list):
        info_str = "{0}님은 계좌가 없어요. 먼저 계좌를 생성하세요.".format(ctx.author.mention)
        embed = embeder(title="구룩코인 - 채굴", desc=info_str)
        await ctx.send(embed = embed)
        return
    else:
        index = moneygame.CoinAccount.uid_list.index(uid)
        result = moneygame.CoinAccount.instance_list[index].mine()
        if(type(result) == int) and (result == -1):
            info_str = "{0}님, 아직 그래픽카드가 수리중입니다.".format(ctx.author.mention)
            embed = embeder(title="구룩코인 - 채굴", desc=info_str)
            await ctx.send(embed = embed)
        else:
            balance_str = str(result[2])
            money_str = str(result[1])
            if result[0] == 1:
                info_str = "{}님이 채굴로 얻은 금액: ".format(ctx.author.mention) + money_str + "GC\n\n" + "현재 잔액: " + balance_str + "GC\n\n" + "그래픽카드는 아직 잘 동작중입니다!"
                embed = embeder(title="구룩코인 - 채굴", desc=info_str)
                await ctx.send(embed = embed)
            elif result[0] == -2:
                info_str = "{}님이 채굴로 얻은 금액: ".format(ctx.author.mention) + money_str + "GC\n\n" + "현재 잔액: " + balance_str + "GC\n\n" + "저런! 그래픽카드가 고장났습니다. 수리되기 전에는 채굴할 수 없어요. 그래픽카드는 1분 후 수리됩니다.\n\n" + "쿨다운 끝: " + result[3]
                embed = embeder(title="구룩코인 - 채굴", desc=info_str)
                await ctx.send(embed = embed)

            moneygame.CoinAccount.instance_list[index].save()

@gc.command(pass_context = True)
async def 업그레이드(ctx):
    uid = ctx.author.id
    transaction = False

    if not(uid in moneygame.CoinAccount.uid_list):
        info_str = "{0}님은 계좌가 없어요. 먼저 계좌를 생성하세요.".format(ctx.author.mention)
        embed = embeder(title="구룩코인 - 업그레이드", desc=info_str)
        await ctx.send(embed = embed)
    
    else:
        index = moneygame.CoinAccount.uid_list.index(uid)
        if moneygame.CoinAccount.instance_list[index].gpu == moneygame.CoinAccount.gpu_max_grade:
            info_str = "{0}님은 지금 최고 등급 그래픽카드에요.".format(ctx.author.mention)
            embed = embeder(title="업그레이드 불가", desc=info_str)
            await ctx.send(embed = embed)
            return

        info_str = "{0}님, 정말로 다음 단계 그래픽카드로 업그레이드 할까요?\n맞다면 **네**라고 20초 내에 답해주세요.\n취소하고 싶으시다면 **아니오**를 입력하세요.".format(ctx.author.mention)
        gpu_grade = moneygame.CoinAccount.instance_list[index].gpu
        next_gpu_info = moneygame.CoinAccount.gpu_info[gpu_grade + 1]
        info_str = info_str + "\n\n다음 등급 GPU 정보\n\n이름: {}\n채굴 범위 (GC): {}\n가격:{}".format(next_gpu_info[0], next_gpu_info[1], next_gpu_info[2])
        embed = embeder(title="업그레이드 확인", desc=info_str)
        await ctx.send(embed = embed)

        def check(m):
            nonlocal transaction
            if m.author == ctx.author and (m.content == "네"):
                transaction = True
                return True
            elif m.author == ctx.author and (m.content == "아니오"):
                transaction = False
                return True
            else:
                return False

        try:
            msg = await bot.wait_for('message', check=check, timeout=20.0)
        except asyncio.TimeoutError:
            transaction = False
        
    if transaction == True:
        result = moneygame.CoinAccount.instance_list[index].upgrade_gpu()
        if result == 'M':
            info_str = "{0}님, 마이너스 한도인 {1}GC를 초과하기 때문에 업그레이드할 수 없어요.".format(ctx.author.mention, moneygame.CoinAccount.minus_limit)
        elif result == 'F':
            info_str = "{0}님의 계좌는 동결되어 있어요.".format(ctx.author.mention)
        else:
            moneygame.CoinAccount.instance_list[index].save()
            info_str = "{}님의 그래픽카드가 업그레이드 되었습니다. 내정보에서 확인해보세요.".format(ctx.author.mention)
        embed = embeder(title="구룩코인 - 업그레이드", desc=info_str)
        await ctx.send(embed = embed)

    elif transaction == False:
        info_str = "{0}님, 업그레이드가 취소되었습니다.".format(ctx.author.mention)
        embed = embeder(title="구룩코인 - 업그레이드", desc=info_str)
        await ctx.send(embed = embed)

@bot.group(pass_context = True)
async def adm(ctx):
    return

@adm.command(pass_context = True)
async def help(ctx):
    uid= ctx.author.id
    cid = 'adm.help'
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return
    embed = discord.Embed(title="관리자 명령어 목록", description="**adm get_admin <암호>**: PM에서 사용하십시오.\n\n**adm abandon_admin <암호>**: PM에서 사용하십시오.\n\n**adm ban <이름이나 ID>**: 유저의 봇 사용을 막습니다.\n\n**adm unban <이름이나 ID>**: 유저의 봇 사용을 다시 허용합니다.\n\n**(최상위 관리자용) adm grant(dismiss)_admin <이름이나 ID>**\n\n**adm reload**: 권한 데이터를 다시 로딩합니다.\n\n")

    await ctx.send(embed = embed)

@adm.command(pass_context = True)
async def reload(ctx):
    uid= ctx.author.id
    cid = 'adm.reload'
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    perms.permissions_reload()
    await ctx.send('✅ 권한을 다시 로드했습니다.')

@adm.command(pass_context = True)
async def ban(ctx, name: discord.Member):

    uid = ctx.author.id
    cid = 'adm.ban'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    if(uid == name.id):
        await ctx.send("⚠️ 스스로 차단할 수 없어요.")
        return

    state = perms.permissions_user_edit(name.id, name, 'nonuser')
    if (state == -2):
        await ctx.send(perm_error)
        return

    await ctx.send('✅ 유저 {0} ({1}) 의 봇 사용이 차단되었어요.'.format(name, name.id))

@adm.command(pass_context = True)
async def unban(ctx, name: discord.Member):

    uid = ctx.author.id
    cid = 'adm.unban'
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    perms.permissions_user_edit(name.id, name, 'user')
    await ctx.send('✅ 유저 {0} ({1}) 의 봇 사용 차단이 해제되었어요'.format(name, name.id))

@adm.command(pass_context = True)
async def get_admin(ctx, password: str):
    uid = ctx.author.id
    if(perms.permissions_config['admin_password'] == password):
        perms.permissions_user_edit(uid, ctx.author, 'admin')
        perms.permissions_reload()
        await ctx.send("✅ 당신은 이제 관리자에요.")
        return
    else:
        return

@adm.command(pass_context = True)
async def abandon_admin(ctx, password: str):
    cid = 'adm.abandon_admin'
    uid= ctx.author.id
    
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    if(perms.permissions_config['admin_password'] == password):
        perms.permissions_user_edit(uid, ctx.author, 'user')
        perms.permissions_reload()
        await ctx.send("✅ 당신의 관리자 권한이 제거되었어요.")
        return
    else:
        return

@adm.command(pass_context = True)
async def get_operator(ctx, password: str):
    uid= ctx.author.id
    if(perms.permissions_config['operator_password'] == password):
        perms.permissions_user_edit(uid, ctx.author, 'operator')
        perms.scramble_op_password()
        await ctx.send("✅ 당신은 이제 최상위 관리자에요. 최상위 관리자 비밀번호가 자동으로 변경되었어요.")
        return
    else:
        return

@adm.command(pass_context = True)
async def abandon_operator(ctx, password: str):
    cid = 'adm.abandon_operator'
    uid= ctx.author.id
    
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    if(perms.permissions_config['operator_password'] == password):
        perms.permissions_user_edit(uid, ctx.author, 'user')
        perms.permissions_reload()
        perms.scramble_op_password()
        await ctx.send("✅ 당신의 최상위 관리자 권한이 제거되었어요. 최상위 관리자 비밀번호가 자동으로 변경되었어요.")
        return
    else:
        return

@adm.command(pass_context = True)
async def grant_admin(ctx, name: discord.Member):
    cid = 'adm.grant_admin'
    uid= ctx.author.id
    
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    perms.permissions_user_edit(name.id, name, 'admin')
    perms.permissions_reload()
    await ctx.send("✅ 유저 {0} ({1}) 은(는) 이제 관리자에요.".format(name, name.id))
    return

@adm.command(pass_context = True)
async def dismiss_admin(ctx, name: discord.Member):
    cid = 'adm.dismiss_admin'
    uid = ctx.author.id
    
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    perms.permissions_user_edit(name.id, name, 'user')
    perms.permissions_reload()
    await ctx.send("✅ User {0} ({1}) 의 관리자 권한이 제거되었어요.".format(name, name.id))
    return

@adm.command(pass_context = True)
async def reload_module(ctx):
    cid = 'adm.reload_module'
    uid = ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    imp.reload(ReadEngine)
    imp.reload(CommandEngine)
    imp.reload(perms)
    imp.reload(silverhair)
    imp.reload(dusthan)
    imp.reload(helper)
    #imp.reload(guruktube)
    imp.reload(moneygame)
    #imp.reload(urlshort)

    await ctx.send("✅ 봇의 구성 요소를 다시 로드했어요.")

@adm.command(pass_context = True)
async def reload_help(ctx):
    cid = 'adm.reload_help'
    uid = ctx.author.id
    
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    helper.load()
    await ctx.send("✅ 도움말을 다시 로드했어요.")

@adm.command(pass_context = True)
async def add_help_cmd(ctx, cmd, string):
    cid = 'adm.add_help_cmd'
    uid = ctx.author.id
    
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    helper.cmd_help_write(cmd, string)
    await ctx.send("✅ 항목이 추가되었어요.")

@adm.command(pass_context = True)
async def delete_help_cmd(ctx, cmd):
    cid = 'adm.delete_help_cmd'
    uid = ctx.author.id
    
    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    helper.cmd_help_delete(cmd)
    await ctx.send("✅ 항목이 삭제되었어요.")

@adm.command(pass_context = True)
async def shutdown(ctx):
    cid = "adm.shutdown"
    uid = ctx.author.id

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return
    await ctx.send("👋 종료할게요.")
    sys.exit(0)
    #await bot.logout()

@adm.command(pass_context = True)
async def 금지추가(ctx):

    uid= ctx.author.id
    cid = 'adm.autoremove'
    

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[10:]
    
    if (msg_str[0] == '"'):
        start = msg_str.find('"') + 1
        end = msg_str.find('"', 1)
        left = msg_str[start:end]
        right = msg_str[(end+1):]
    else:
        start = 0
        end = msg_str.find(" ")
        left = msg_str[start:end]
        right = msg_str[(end+1):]



    CommandEngine.write_autoremove(left, right)
    CommandEngine.reload_autoremove()
    await ctx.send('✅ 금지 키워드 `{0}: {1}`가 추가되었어요.'.format(left, right))
    await ctx.send("금지 키워드를 추가할 때에는 항상 주의하세요. 예상치 못한 결과가 발생할 수 있습니다.")


@adm.command(pass_context = True)
async def 금지삭제(ctx, cmd: str):
    uid= ctx.author.id
    cid = 'adm.autoremove'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    msg_str = ctx.message.content[10:]

    result = CommandEngine.delete_autoremove(msg_str)
    if result == -1:
        await ctx.send('⚠️ 사용자 이미지태그 `{0}`는 없어요.'.format(msg_str))
    else:
        await ctx.send('✅ 사용자 이미지태그 `{0}`이(가) 삭제되었어요.'.format(msg_str))

@adm.command(pass_context = True)
async def 금지목록(ctx, page = 1):
    uid= ctx.author.id
    cid = 'adm.autoremove'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return

    command_key = list(CommandEngine.autoremove_diction.keys())
    command_content = list(CommandEngine.autoremove_diction.values())
    page_str = '✅ 명령어 수: {0}개\n**<태그 : 파라미터>**\n파라미터가 0이면 삭제 시 로그를 표시하지 않으며, 1이면 표시해요.\n'.format(len(CommandEngine.autoremove_diction))

    max_pages = (len(command_key) // 15)
    if (max_pages * 15 < len(command_key) ):
        max_pages += 1

    if (page > max_pages):
        page = max_pages
    elif (page < 1):
        page = 1

    for i in range(15 * (page - 1), (15 * page), 1):
        if (i >= len(command_key)):
            break
        page_str = page_str + "`" + command_key[i] + ' : ' + command_content[i] + "`" +'\n'

    if (len(page_str)>2000):
        await ctx.send("내용이 너무 길어 생략되었어요.")
        page_str = '✅ 명령어 수: {0}개\n**<태그 : 파라미터>**\n파라미터가 0이면 삭제 시 로그를 표시하지 않으며, 1이면 표시해요.\n'.format(len(CommandEngine.autoremove_diction))

        for i in range(15 * (page - 1), (15 * page), 1):
            if (i >= len(command_key)):
                break
            page_str = page_str + command_key[i] + '\n'


    embed = embeder("사용자 태그 목록", "", page_str, "", 0x3962e0, "<{0} 페이지 중 {1} 페이지>".format(max_pages, page))
    await ctx.send(embed = embed)

@adm.command(pass_context = True)
async def 금지강제삭제(ctx):
    uid= ctx.author.id
    cid = 'adm.autoremove'

    if (perms.permissions_manager(uid, cid) == -1):
        await ctx.send(perm_error)
        return


    result = CommandEngine.force_delete_autoremove()
    if result == -1:
        await ctx.send('⚠️ 강제 삭제에 실패했어요.')
    else:
        await ctx.send('✅ 목록의 마지막 키워드가 삭제되었어요.')
    

########################################################################################################
@bot.event
async def on_message(message):
    uid= message.author.id

    if ((message.content.startswith(prefix)) and (message.author != bot.user) ):
        content = message.content[len(prefix):]

        if content.startswith(prefix):
            if content[len(prefix):] in CommandEngine.custom_imgtag_diction.keys():
                cid = 'img'
                if (perms.permissions_manager(uid, cid) == -1):
                    await message.channel.send(perm_error)
                    return

                tag = CommandEngine.custom_imgtag_diction[content[len(prefix):]]
                (imglink, postid) = silverhair.search(tag)
                safebooru_url = "https://safebooru.org/index.php?page=post&s=view&id=" + postid
                embed = embeder(title = "Safebooru", url = safebooru_url, desc = "", image = imglink, color = 0x3962e0, footer = "짤검색")
                await message.channel.send(embed=embed)

        if content in CommandEngine.custom_commands_diction.keys():
            cid = '명령사용'

            if (perms.permissions_manager(uid, cid) == -1):
                await message.channel.send(perm_error)
                return

            await message.channel.send(CommandEngine.custom_commands_diction[content])
            
            #if(CommandEngine.custom_commands_diction[content].startswith(tuple(url_head)) and CommandEngine.custom_commands_diction[content].endswith(tuple(img_ext))):
            #    embed = discord.Embed(title=content, url=CommandEngine.custom_commands_diction[content])
            #    embed.set_footer(text="Auto-embed")
            #    embed.set_image(url=CommandEngine.custom_commands_diction[content])
            #    await message.channel.send(embed = embed)

            #else:
            #    await message.channel.send(CommandEngine.custom_commands_diction[content])

        elif (message.content.find('로그정리') == len(prefix)):
            cid = '로그정리'
            msg_count = 0
            if (perms.permissions_manager(uid, cid) == -1):
                await message.channel.send(perm_error)
                return

            if type(message.channel) == discord.TextChannel:
                async for msg in message.channel.history():
                    if (msg.author == bot.user):
                        await msg.delete()
                        msg_count = msg_count + 1
            elif type(message.channel) == discord.DMChannel:
                async for msg in message.channel.history():
                    if (msg.author == bot.user):
                        await msg.delete()
                        msg_count = msg_count + 1

            await message.channel.send('✅ 현재 채널의 메시지중에서 메시지 {0}개를 정리했어요.'.format(msg_count))

        elif (message.content.find('따라해') == len(prefix)):
            cid = '따라해'
            if (perms.permissions_manager(uid, cid) == -1):
                await message.channel.send(perm_error)
                return

            await message.channel.send(message.content[(len(prefix) + len('따라해')):])

    elif (not(message.content.startswith(prefix)) and (message.author != bot.user) ):
        content = message.content[0:]

        for key in CommandEngine.autoremove_diction:
            if key in content:
                if CommandEngine.autoremove_diction[key] == '1':
                    await message.channel.send("금지 키워드로 설정된 단어가 있어 메시지를 삭제했어요.")
                await message.delete()
                return


        for key in CommandEngine.custom_keywords_diction:
            if key in content:
                cid = '키워드사용'
                if (perms.permissions_manager(uid, cid) == -1):
                    #await message.channel.send(perm_error)
                    return

                await message.channel.send(CommandEngine.custom_keywords_diction[key])

    await bot.process_commands(message)


##########################################################
bot.run(tokens['bot_token'])

