# -*_ coding: utf-8 -*-
#구룩이봇 세이프부루 확장

from mmap import PAGESIZE
import requests
import re
import json
import random
import xml.etree.ElementTree
from datetime import datetime

pic_list_raw = []
pic_list = []
count = 0
choice_old = 0

pic_list_raw_s = []
pic_list_s = []
choice_old_s = 0

def silverhair():
    global count
    global pic_list
    global pic_list_raw
    global choice_old

    random.seed(datetime.now())

    if count == 10:
        count = 0

    if count == 0:
        pic_list_rawstring = requests.get(url = 'https://safebooru.org/index.php?page=dapi&s=post&q=index&limit={0}&pid={1}&tags={2}'.format('100', random.randint(1, 2000), 'silver_hair'))
        status = pic_list_rawstring.status_code

        if (status != 200):
            return -1

        pic_list_xml = xml.etree.ElementTree.fromstring(pic_list_rawstring.text)
        pic_list_raw = pic_list_xml.findall("post")
        count = count + 1

        for i in pic_list_raw:
            #print(i.attrib["file_url"])
            pic_list.append(i.attrib["file_url"])

    choice = random.randint(0, len(pic_list) - 1)

    while(choice == choice_old):
        choice = random.randint(0, len(pic_list) - 1)

    choice_old = choice
    #print('선택된 이미지: ', pic_list[choice])

    return (pic_list[choice])

def cat():
    url = 'https://api.thecatapi.com/v1/images/search'
    response = requests.get(url = url)
    data = json.loads(response.text)
    
    return (data[0]['url'])

def search(tag: str):
    pic_list_s = []
    id_list_s = []
    random.seed(datetime.now())

    tag = tag + "+-male_focus+-male+-1boy+-2boys+-3boys+-4boys+-5boys+-furry"

    pic_list_rawstring = requests.get(url = 'https://safebooru.org/index.php?page=dapi&s=post&q=index&limit={0}&tags={1}'.format('0', tag))
    status = pic_list_rawstring.status_code

    if (status != 200):
        return -1

    sizestart = pic_list_rawstring.text.find("count")
    sizeend = pic_list_rawstring.text.find("offset")
    size = int(re.findall("\d+", pic_list_rawstring.text[sizestart:sizeend])[0])
    page = size//100

    pic_list_rawstring = requests.get(url = 'https://safebooru.org/index.php?page=dapi&s=post&q=index&limit={0}&tags={1}&pid={2}'.format('100', tag, random.randint(0, page)))
    pic_list_xml = xml.etree.ElementTree.fromstring(pic_list_rawstring.text)
    pic_list_raw_s = pic_list_xml.findall("post")

    for i in pic_list_raw_s:
        #print(i.attrib["file_url"])
        #pic_list_s.append(i.attrib["file_url"])
        pic_list_s.append(i.attrib["sample_url"])
        id_list_s.append(i.attrib["id"])

    choice = random.randint(0, len(pic_list_s) - 1)

    return (pic_list_s[choice], id_list_s[choice])

#print("작업을 마침.")