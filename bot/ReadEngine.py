# -*_ coding: utf-8 -*-
#구룩이봇 파일 I/O모듈 (ReadEngine)
import string

version = "1.0 (190829)"

row_delimiter = '│'
column_delimiter = '\n'
newline_replacement = '┛'
#내부 구분자

def Read_To_String(filedir: str):
    try:
        target = open(filedir, 'r', encoding = 'utf-8')
    except:
        target = open(filedir, 'w', encoding = 'utf-8')
        target.close()
        target = open(filedir, 'r', encoding = 'utf-8')
    else:
        target_string = target.read()
        target.close()
        #target_string = target_string.replace(newline_replacement, '\n')
        return target_string

def Read_To_Diction(filedir: str):
    temp_list1 = []
    temp_list2 = []
    target_diction = {}

    target_string = Read_To_String(filedir)
    temp_list1 = target_string.split(column_delimiter)
    
    for i in range(0, len(temp_list1)-1, 1):
        temp_list2 = temp_list1[i].replace(newline_replacement, '\n').split(row_delimiter)
        target_diction[temp_list2[0]] = temp_list2[1]
    
    return target_diction


def Add_To_Text(filedir: str, str1: str, str2: str):
    target = open(filedir, 'a', encoding = 'utf-8')
    target.write('{0}{1}{2}{3}'.format(str1, row_delimiter, str2.replace('\n', newline_replacement), column_delimiter))
    target.close()

def Write_To_Text(filedir: str, str1: str, str2: str):
    target = open(filedir, 'w', encoding = 'utf-8')
    target.write('{0}{1}{2}{3}'.format(str1, row_delimiter, str2.replace('\n', newline_replacement), column_delimiter))
    target.close()

def Write_Diction_To_Text(filedir: str, target_diction: dict):
    target = open(filedir, 'w', encoding = 'utf-8')
    target.close()
    
    for key in target_diction.keys():
        Add_To_Text(filedir, key, target_diction[key])