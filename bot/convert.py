# -*_ coding: utf-8 -*-
#190826. 6 이전 버전의 커맨드 및 키워드 형식을 새로운 형식으로 변환.
import string

file = open("./commands_list.txt", "r", encoding = "utf-8")
string = file.read()
string = string.replace("╂", "\n")
file = open("./commands_list.txt", "w", encoding = "utf-8")
file.write(string)

file = open("./keywords_list.txt", "r", encoding = "utf-8")
string = file.read()
string = string.replace("╂", "\n")
file = open("./keywords_list.txt", "w", encoding = "utf-8")
file.write(string)
file.close()

print("변환을 완료했습니다.")
