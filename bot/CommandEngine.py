# -*_ coding: utf-8 -*-
#구룩이봇 커스텀 커맨드 키워드 모듈 (CommandEngine)

import ReadEngine

custom_commands_diction = {}
custom_keywords_diction = {}
custom_imgtag_diction = {}
autoremove_diction = {}


def load_custom_commands():
    global custom_commands_diction
    custom_commands_diction = ReadEngine.Read_To_Diction('./db/commands_list.txt')

def write_custom_commands(cmd, con):
    ReadEngine.Add_To_Text('./db/commands_list.txt', cmd, con)

def delete_custom_commands(cmd):
    global custom_commands_diction

    try:
        custom_commands_diction.pop(cmd)
    except:
        return -1
    else:
        ReadEngine.Write_Diction_To_Text('./db/commands_list.txt', custom_commands_diction)
        return 0

def reload_custom_commands():
    global custom_commands_diction
    custom_commands_diction.clear()
    load_custom_commands()


def load_custom_keywords():
    global custom_keywords_diction
    custom_keywords_diction = ReadEngine.Read_To_Diction('./db/keywords_list.txt')

def write_custom_keywords(cmd, con):
    ReadEngine.Add_To_Text('./db/keywords_list.txt', cmd, con)

def delete_custom_keywords(cmd):
    global custom_keywords_diction

    try:
        custom_keywords_diction.pop(cmd)
    except:
        return -1
    else:
        ReadEngine.Write_Diction_To_Text('./db/keywords_list.txt', custom_keywords_diction)
        return 0

def reload_custom_keywords():
    global custom_keywords_diction
    custom_keywords_diction.clear()
    load_custom_keywords()


def load_custom_imgtag():
    global custom_imgtag_diction
    custom_imgtag_diction = ReadEngine.Read_To_Diction('./db/imgtag_list.txt')

def write_custom_imgtag(cmd, con):
    ReadEngine.Add_To_Text('./db/imgtag_list.txt', cmd, con)

def delete_custom_imgtag(cmd):
    global custom_imgtag_diction

    try:
        custom_imgtag_diction.pop(cmd)
    except:
        return -1
    else:
        ReadEngine.Write_Diction_To_Text('./db/imgtag_list.txt', custom_imgtag_diction)
        return 0

def reload_custom_imgtag():
    global custom_imgtag_diction
    custom_imgtag_diction.clear()
    load_custom_imgtag()


def load_autoremove():
    global autoremove_diction
    autoremove_diction = ReadEngine.Read_To_Diction('./db/autoremove_list.txt')

def write_autoremove(cmd, con):
    ReadEngine.Add_To_Text('./db/autoremove_list.txt', cmd, con)

def delete_autoremove(cmd):
    global autoremove_diction

    try:
        autoremove_diction.pop(cmd)
    except:
        return -1
    else:
        ReadEngine.Write_Diction_To_Text('./db/autoremove_list.txt', autoremove_diction)
        return 0

def force_delete_autoremove():
    global autoremove_diction

    try:
        last_key = list(autoremove_diction.keys())[len(autoremove_diction) - 1]
        autoremove_diction.pop(last_key)
    except:
        return -1
    else:
        ReadEngine.Write_Diction_To_Text('./db/autoremove_list.txt', autoremove_diction)
        return 0


def reload_autoremove():
    global autoremove_diction
    autoremove_diction.clear()
    load_autoremove()

load_custom_commands()
load_custom_keywords()
load_custom_imgtag()
load_autoremove()